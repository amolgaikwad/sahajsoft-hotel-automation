package com.sahajsoft.hotelautomation.automation;

import com.sahajsoft.hotelautomation.hotel.CorridorType;
import com.sahajsoft.hotelautomation.hotel.Hotel;
import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rule;
import org.jeasy.rules.api.Rules;
import org.jeasy.rules.api.RulesEngine;
import org.jeasy.rules.core.DefaultRulesEngine;

import java.util.Stack;

public class HotelAutomation {

    static final String MOVEMENT_INPUT = "MovementInput";
    private Rules rules;
    private Hotel hotel;
    private RulesEngine rulesEngine;
    private Facts facts;

    public HotelAutomation(final Hotel hotel, final Rules rules) {
        rulesEngine = new DefaultRulesEngine();
        facts = new Facts();
        facts.put(MOVEMENT_INPUT, new Stack<MovementInput>());
        this.hotel = hotel;
        this.rules = rules;
        runDefaultRules();
    }

    public void run(final MovementInput movementInput) {
        Rules actionRules = fetchRules(RuleType.ACTION);
        facts.put(identifyCurrentSlot().name(), null);

        registerMovement(movementInput);
        createRegisterForCorridorSpecificMovement(movementInput);

        rulesEngine.fire(actionRules, facts);
    }

    private void createRegisterForCorridorSpecificMovement(MovementInput movementInput) {
        if(!facts.asMap().containsKey(movementInput.toString())){
            facts.put(movementInput.toString(), new Stack<>());
        }
    }

    private void registerMovement(MovementInput movementInput) {
        Stack stack = facts.get(MOVEMENT_INPUT);
        stack.push(movementInput);
    }

    public String retrieveHotelState() {
        return hotel.toString();
    }

    public void run() {
        Rules actionRules = fetchRules(RuleType.ACTION);
        facts.put(identifyCurrentSlot().name(), null);
        rulesEngine.fire(actionRules, facts);
    }

    private void runDefaultRules() {
        if (isNoRuleDefined()) {
            return;
        }
        Rules defaultRules = fetchRules(RuleType.DEFAULT);
        rulesEngine.fire(defaultRules, new Facts());
    }

    private boolean isNoRuleDefined() {
        return rules == null || rules.isEmpty();
    }

    private TimeSlot identifyCurrentSlot() {
        //Based on time evaluate slots.
        return TimeSlot.NIGHT_TIME_SLOT;
    }

    private Rules fetchRules(final RuleType type) {
        Rules rulesToBeExecuted = new Rules();

        for (Rule tempRule : rules) {
            if (tempRule.getDescription().equalsIgnoreCase(type.name())) {
                rulesToBeExecuted.register(tempRule);
            }
        }
        return rulesToBeExecuted;
    }

    public Integer evaluateConsumption() {
        return hotel.evaluateConsumption();
    }

    public static class MovementInput {
        private final int floorNumber;
        private final int subCorridorNumber;
        private final CorridorType corridorType;

        MovementInput(final int floorNumber, final int subCorridorNumber, final CorridorType corridorType) {
            this.floorNumber = floorNumber;
            this.subCorridorNumber = subCorridorNumber;
            this.corridorType = corridorType;
        }

        public int getFloorNumber() {
            return floorNumber;
        }

        public int getSubCorridorNumber() {
            return subCorridorNumber;
        }

        public CorridorType getCorridorType() {
            return corridorType;
        }

        @Override
        public String toString() {
            return "floorNumber::"+floorNumber
                   + " subCorridorNumber::" + subCorridorNumber
                   + " corridorType::" + corridorType;
        }
    }

    enum RuleType {DEFAULT, ACTION}

    enum TimeSlot {NIGHT_TIME_SLOT}
}
