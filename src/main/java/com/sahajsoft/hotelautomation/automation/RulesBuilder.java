package com.sahajsoft.hotelautomation.automation;

import com.sahajsoft.hotelautomation.equipment.EquipmentType;
import com.sahajsoft.hotelautomation.hotel.CorridorType;
import com.sahajsoft.hotelautomation.hotel.Hotel;
import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rules;
import org.jeasy.rules.core.RuleBuilder;

import java.util.Arrays;
import java.util.Stack;
import java.util.Timer;
import java.util.TimerTask;

import static com.sahajsoft.hotelautomation.automation.HotelAutomation.*;
import static com.sahajsoft.hotelautomation.automation.HotelAutomation.MOVEMENT_INPUT;

class RulesBuilder {
    private Rules rules;

    RulesBuilder() {
        rules = new Rules();
    }

    Rules build() {
        return rules;
    }

    RulesBuilder turnOnAllACsRule(final Hotel hotel, final RuleType ruleType) {
        rules.register(
                new RuleBuilder()
                .name("turnOnAC")
                .when(condition -> true)
                .description(ruleType.name())
                .then(action -> hotel.turnOnAll(EquipmentType.AC, Arrays.asList(CorridorType.SUB, CorridorType.MAIN)))
                .build());
        return this;
    }

    RulesBuilder turnOnAllMainCorridorLightsDuringNightSlotRule(Hotel hotel, RuleType ruleType) {
        rules.register(
                new RuleBuilder()
                .name("turnOnMainCorridorLights")
                .description(ruleType.name())
                .when(facts -> isNightTimeSlot(facts))
                .then(action -> hotel.turnOnAll(EquipmentType.LIGHT, Arrays.asList(CorridorType.MAIN)))
                .build());
        return this;
    }

    RulesBuilder turnOnCorridorLightsDuringNightSlotForMovementRule(Hotel hotel, RuleType ruleType) {
        rules.register(
                new RuleBuilder()
                .name("turnOnLightsOnMovement")
                .description(ruleType.name())
                .when(facts -> isNightTimeSlot(facts) &&
                        isThereAMovementInCorridor(facts))
                .then(facts -> {
                    MovementInput movementInput = getMovementInput(facts);

                    hotel.turnOnEquipments(new Hotel.EquipmentSwitchInput(EquipmentType.LIGHT, movementInput.getFloorNumber(),
                            movementInput.getCorridorType(), movementInput.getSubCorridorNumber()));

                    registerCorridorSpecificMovement(facts, movementInput);

                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            if(isThereNoMovementInTheSameCorridor(movementInput)){
                                hotel.turnOffEquipmentsIn(
                                        new Hotel.EquipmentSwitchInput(EquipmentType.LIGHT, movementInput.getFloorNumber(), movementInput.getCorridorType(), movementInput.getSubCorridorNumber()));
                            }
                        }

                        private boolean isThereNoMovementInTheSameCorridor(MovementInput movementInput) {
                            Stack requestStack = facts.get(movementInput.toString());
                            requestStack.pop();
                            if(requestStack.isEmpty()){
                                return true;
                            }
                            else {
                                return false;
                            }
                        }
                    }, 5000);

                })
                .build());
        return this;
    }

    private void registerCorridorSpecificMovement(Facts facts, MovementInput movementInput) {
        Stack requestStack = facts.get(movementInput.toString());
        requestStack.push(new Object());
    }

    private MovementInput getMovementInput(Facts facts) {
        Stack<MovementInput> stack = facts.get(MOVEMENT_INPUT);
        return stack.pop();
    }

    private boolean isThereAMovementInCorridor(Facts facts) {
        Stack<MovementInput> stack = facts.get(MOVEMENT_INPUT);
        return !stack.isEmpty();
    }

    private boolean isNightTimeSlot(Facts facts) {
        return facts.asMap().containsKey(TimeSlot.NIGHT_TIME_SLOT.name());
    }
}
