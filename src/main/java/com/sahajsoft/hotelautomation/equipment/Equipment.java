package com.sahajsoft.hotelautomation.equipment;

public class Equipment {
    private int unitConsumption;
    private boolean isPowerOn;
    private EquipmentType equipmentType;

    public Equipment(final EquipmentType equipmentType, final int unitConsumption) {
        this.equipmentType = equipmentType;
        this.unitConsumption = unitConsumption;
        this.isPowerOn = false;
    }

    public void turnPowerOn(){
        isPowerOn = true;
    }

    public void turnPowerOff(){
        isPowerOn = false;
    }

    public EquipmentType getEquipmentType() {
        return equipmentType;
    }

    @Override
    public String toString() {
        String switchStatus;
        if (isPowerOn){
            switchStatus = "ON";
        }else{
            switchStatus = "OFF";
        }
        return " " + equipmentType.getName() + " : " + switchStatus + " ";
    }

    public Integer evaluateConsumption() {
        if(!isPowerOn){
            return 0;
        }
        return unitConsumption;
    }
}
