package com.sahajsoft.hotelautomation.equipment;

public enum EquipmentType{
    AC("AC"), LIGHT("Light");

    private final String name;

    EquipmentType(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }
}
