package com.sahajsoft.hotelautomation.hotel;

public enum CorridorType {

    MAIN("Main"), SUB("Sub");
    private String type;

    CorridorType(String type) {
        this.type = type;
    }

    String getType() {
        return type;
    }
}
