package com.sahajsoft.hotelautomation.hotel;

import com.sahajsoft.hotelautomation.equipment.Equipment;
import com.sahajsoft.hotelautomation.equipment.EquipmentType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

public class Hotel {

    private Map<Integer, Floor> floors;

    private Hotel(final HotelBuilder hotelBuilder) {
        this.floors = hotelBuilder.floors;
    }

    public String toString() {
        StringBuilder state = new StringBuilder();
        floors.forEach((key, value) -> state.append(value.toString()));
        return state.toString();
    }

    public void install(final InstallEquipmentInput installEquipmentInput) {
        Floor floor = floors.get(installEquipmentInput.getFloorNumber());
        floor.install(installEquipmentInput.getEquipments(), new Corridor(installEquipmentInput.getCorridorNumber(), installEquipmentInput.getCorridorType()));
    }

    public void turnOnEquipments(final EquipmentSwitchInput equipmentSwitchInput) {
        Floor floor = floors.get(equipmentSwitchInput.getFloorNumber());
        floor.turnOnEquipmentsIn(equipmentSwitchInput.getType(),
                new Corridor(equipmentSwitchInput.getSubCorridorNumber(), equipmentSwitchInput.getCorridorType()));
    }

    public void turnOffEquipmentsIn(final EquipmentSwitchInput equipmentSwitchInput) {
        Floor floor = floors.get(equipmentSwitchInput.getFloorNumber());
        floor.turnOffEquipmentsIn(equipmentSwitchInput.getType(),
                new Corridor(equipmentSwitchInput.getSubCorridorNumber(), equipmentSwitchInput.getCorridorType()));
    }

    public void turnOnAll(EquipmentType equipmentType, List<CorridorType> applicableCorridors) {
        floors.values().forEach(floor -> floor.turnOnAll(equipmentType, applicableCorridors));
    }

    public Integer evaluateConsumption() {
        AtomicReference<Integer> consumption = new AtomicReference<>();
        consumption.set(0);
        floors.values().forEach(f-> consumption.set(consumption.get() + f.evaluateConsumption()));
        return consumption.get();
    }

    public static class HotelBuilder {

        private Map<Integer, Floor> floors;

        public HotelBuilder(final int numberOfFloors, final int numberOfMainCorridorsPerFloor,
                            final int numberOfSubCorridorsPerFloor) {
            validateInputs(numberOfFloors, numberOfMainCorridorsPerFloor, numberOfSubCorridorsPerFloor);
            floors = new HashMap<>();
            for (int floorNumber = 1; floorNumber <= numberOfFloors; floorNumber++) {
                List<Corridor> corridors = new ArrayList<>();

                corridors.addAll(buildCorridors(numberOfMainCorridorsPerFloor, CorridorType.MAIN));
                corridors.addAll(buildCorridors(numberOfSubCorridorsPerFloor, CorridorType.SUB));

                floors.put(floorNumber, (new Floor(floorNumber, corridors)));
            }
        }

        private void validateInputs(final int numberOfFloors, final int numberOfMainCorridorsPerFloor, final int numberOfSubCorridorsPerFloor) {
            if (numberOfFloors <= 0 || numberOfMainCorridorsPerFloor < 0 || numberOfSubCorridorsPerFloor < 0) {
                throw new IllegalArgumentException("Invalid inputs provided!");
            }
        }

        public Hotel build() {
            return new Hotel(this);
        }

        private List<Corridor> buildCorridors(final int numberOfMainCorridorsPerFloor, final CorridorType corridorType) {
            List<Corridor> corridors = new ArrayList<>();
            for (int corridorNumber = 1; corridorNumber <= numberOfMainCorridorsPerFloor; corridorNumber++) {
                corridors.add(new Corridor(corridorNumber, corridorType));
            }
            return corridors;
        }
    }

    public static class EquipmentSwitchInput {
        private final EquipmentType type;
        private final int floorNumber;
        private final CorridorType corridorType;
        private final int subCorridorNumber;

        public EquipmentSwitchInput(final EquipmentType type, final int floorNumber,
                                    final CorridorType corridorType, final int subCorridorNumber) {
            this.type = type;
            this.floorNumber = floorNumber;
            this.corridorType = corridorType;
            this.subCorridorNumber = subCorridorNumber;
        }

        EquipmentType getType() {
            return type;
        }

        int getFloorNumber() {
            return floorNumber;
        }

        CorridorType getCorridorType() {
            return corridorType;
        }

        int getSubCorridorNumber() {
            return subCorridorNumber;
        }
    }

    public static class InstallEquipmentInput {
        private final List<Equipment> equipments;
        private final int floorNumber;
        private final CorridorType corridorType;
        private final int corridorNumber;

        public InstallEquipmentInput(final List<Equipment> equipments, final int floorNumber,
                                     final CorridorType corridorType, final int corridorNumber) {
            this.equipments = equipments;
            this.floorNumber = floorNumber;
            this.corridorType = corridorType;
            this.corridorNumber = corridorNumber;
        }

        public List<Equipment> getEquipments() {
            return equipments;
        }

        public int getFloorNumber() {
            return floorNumber;
        }

        public CorridorType getCorridorType() {
            return corridorType;
        }

        public int getCorridorNumber() {
            return corridorNumber;
        }
    }
}
