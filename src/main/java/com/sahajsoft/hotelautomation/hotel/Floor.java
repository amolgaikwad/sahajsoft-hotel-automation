package com.sahajsoft.hotelautomation.hotel;

import com.sahajsoft.hotelautomation.equipment.Equipment;
import com.sahajsoft.hotelautomation.equipment.EquipmentType;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

class Floor {
    private List<Corridor> corridors;
    private int floorNumber;

    Floor(final int floorNumber, final List<Corridor> corridors) {
        this.floorNumber = floorNumber;
        this.corridors = corridors;
    }

    @Override
    public String toString() {
        StringBuilder state = new StringBuilder();
        state.append("Floor " + floorNumber + "\n");
        corridors.forEach(corridor -> state.append(corridor.toString()));
        return state.toString();
    }

    void install(final List<Equipment> equipments, final Corridor corridor) {
        Corridor fetchedCorridor = fetchCorridor(corridor);
        fetchedCorridor.install(equipments);
    }

    void turnOnEquipmentsIn(final EquipmentType type, final Corridor corridor) {
        Corridor fetchedCorridor = fetchCorridor(corridor);
        fetchedCorridor.turnOnEquipments(type);
    }

    void turnOffEquipmentsIn(final EquipmentType type, final Corridor corridor) {
        Corridor fetchedCorridor = fetchCorridor(corridor);
        fetchedCorridor.turnOffEquipments(type);
    }

    void turnOnAll(final EquipmentType equipmentName, final List<CorridorType> applicableCorridors) {
        corridors.forEach(corridor -> corridor.turnOnAll(equipmentName, applicableCorridors));
    }

    Integer evaluateConsumption() {
        AtomicReference<Integer> consumption = new AtomicReference<>();
        consumption.set(0);
        corridors.forEach(corridor -> consumption.set(consumption.get() + corridor.evaluateConsumption()));
        return consumption.get();
    }

    private Corridor fetchCorridor(final Corridor corridor) {
        Optional<Corridor> fetchedCorridor = corridors.stream()
                .filter(c -> c.equals(corridor))
                .findFirst();

        if(fetchedCorridor.isPresent()){
            return fetchedCorridor.get();
        }else{
            throw new RuntimeException("Corridor not found!");
        }
    }
}
