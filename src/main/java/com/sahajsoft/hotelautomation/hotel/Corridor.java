package com.sahajsoft.hotelautomation.hotel;

import com.sahajsoft.hotelautomation.equipment.Equipment;
import com.sahajsoft.hotelautomation.equipment.EquipmentType;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

class Corridor {
    private CorridorType type;
    private int corridorNumber;
    private List<Equipment> equipments = new ArrayList<>();

    Corridor(final int corridorNumber, final CorridorType type) {
        this.corridorNumber = corridorNumber;
        this.type = type;
    }

    @Override
    public String toString() {
        StringBuilder state = new StringBuilder();
        state.append(type.getType() + " corridor " + corridorNumber);
        equipments.forEach(e->state.append(e.toString()));
        state.append("\n");
        return state.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Corridor corridor = (Corridor) o;
        return corridorNumber == corridor.corridorNumber &&
                type == corridor.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, corridorNumber);
    }

    void install(final List<Equipment> equipments) {
        this.equipments.addAll(equipments);
    }

    void turnOnEquipments(final EquipmentType type) {
        equipments.stream()
                .filter(equipment -> equipment.getEquipmentType().equals(type))
                .forEach(e -> e.turnPowerOn());
    }

    void turnOffEquipments(final EquipmentType type) {
        equipments.stream()
                .filter(equipment -> equipment.getEquipmentType().equals(type))
                .forEach(e -> e.turnPowerOff());
    }

    void turnOnAll(final EquipmentType equipmentType, final List<CorridorType> applicableCorridors) {
        equipments.stream()
                  .filter(equipment -> equipment.getEquipmentType().equals(equipmentType)
                  && applicableCorridors.contains(type))
                  .forEach(equipment -> equipment.turnPowerOn());
    }

    Integer evaluateConsumption() {
        AtomicReference<Integer> consumption = new AtomicReference<>();
        consumption.set(0);
        equipments.forEach(equipment -> consumption.set(consumption.get() + equipment.evaluateConsumption()));
        return consumption.get();
    }
}
