package com.sahajsoft.hotelautomation.hotel;

import com.sahajsoft.hotelautomation.equipment.Equipment;
import com.sahajsoft.hotelautomation.equipment.EquipmentType;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class HotelTest {

    private Hotel hotel;
    private String expectHotelState;

    @Test
    public void shouldCreateHotelWithGivenInputs() {
        int numberOfFloors = 2;
        int numberOfMainCorridorsPerFloor = 1;
        int numberOfSubCorridorsPerFloor = 2;

        whenBuildHotelWith(numberOfFloors, numberOfMainCorridorsPerFloor, numberOfSubCorridorsPerFloor);

        thenVerifyHotelIsBuildRight();
    }

    @Test
    public void shouldCreateHotelWithFloorsAndMainCorridorOnly() {
        int numberOfFloors = 2;
        int numberOfMainCorridorsPerFloor = 1;
        int numberOfSubCorridorsPerFloor = 0;

        whenBuildHotelWith(numberOfFloors, numberOfMainCorridorsPerFloor, numberOfSubCorridorsPerFloor);

        thenVerifyHotelIsBuiltWithFloorsAndMainCorridor();
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowInvalidInputExceptionOnCreateHotelWithInvalidNumberOfMainCorridor() {
        int numberOfFloors = 1;
        int numberOfMainCorridorsPerFloor = -1;
        int numberOfSubCorridorsPerFloor = 2;

        whenBuildHotelWith(numberOfFloors, numberOfMainCorridorsPerFloor, numberOfSubCorridorsPerFloor);

        assertNull(hotel);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowInvalidInputExceptionOnCreateHotelWithInvalidNumberOfFloors() {
        int numberOfFloors = 0;
        int numberOfMainCorridorsPerFloor = -1;
        int numberOfSubCorridorsPerFloor = 2;

        whenBuildHotelWith(numberOfFloors, numberOfMainCorridorsPerFloor, numberOfSubCorridorsPerFloor);

        assertNull(hotel);
    }

    @Test
    public void shouldInstallLightOnGivenCorridor() {
        givenHotel()
                .withNoEquipmentsInstalled();

        whenInstallLightOn(1, CorridorType.MAIN, 1);

        thenVerifyLightsAreInstalled();
    }

    @Test
    public void shouldInstallLightAndACOnGivenCorridor() {
        givenHotel()
                .withNoEquipmentsInstalled();

        whenInstallLightAndACsOn(1, CorridorType.MAIN, 1);

        thenVerifyLightsAndACsAreInstalled();
    }

    @Test
    public void shouldTurnOnAllACs() {
        givenHotel()
                .withLightsAndACsInstalled();

        whenTurnOn(EquipmentType.AC, Arrays.asList(CorridorType.MAIN, CorridorType.SUB));

        thenVerifyAllACsAreTurnedOn();
    }

    @Test
    public void shouldTurnOnAllLigths() {
        givenHotel()
                .withLightsAndACsInstalled();

        whenTurnOn(EquipmentType.LIGHT, Arrays.asList(CorridorType.MAIN, CorridorType.SUB));

        thenVerifyAllLightsAreTurnedOn();
    }

    @Test
    public void shouldTurnOnAllLightsInMainCorridor() {
        givenHotel()
                .withLightsAndACsInstalled();

        whenTurnOn(EquipmentType.LIGHT, Arrays.asList(CorridorType.MAIN));

        thenVerifyLightsAreTurnedOnMainCorridorOnly();
    }

    private void thenVerifyLightsAreTurnedOnMainCorridorOnly() {
        expectHotelState = "Floor 1\n" +
                "Main corridor 1 Light : ON  AC : OFF \n" +
                "Sub corridor 1 Light : OFF  AC : OFF \n" +
                "Sub corridor 2 Light : OFF  AC : OFF \n" +
                "Floor 2\n" +
                "Main corridor 1 Light : ON  AC : OFF \n" +
                "Sub corridor 1 Light : OFF  AC : OFF \n" +
                "Sub corridor 2 Light : OFF  AC : OFF \n";
        assertEquals(expectHotelState, hotel.toString());
    }

    private void thenVerifyAllLightsAreTurnedOn() {
        expectHotelState = "Floor 1\n" +
                "Main corridor 1 Light : ON  AC : OFF \n" +
                "Sub corridor 1 Light : ON  AC : OFF \n" +
                "Sub corridor 2 Light : ON  AC : OFF \n" +
                "Floor 2\n" +
                "Main corridor 1 Light : ON  AC : OFF \n" +
                "Sub corridor 1 Light : ON  AC : OFF \n" +
                "Sub corridor 2 Light : ON  AC : OFF \n";
        assertEquals(expectHotelState, hotel.toString());
    }

    private void whenTurnOn(EquipmentType ac, List<CorridorType> applicableCorridors) {
        hotel.turnOnAll(ac, applicableCorridors);
    }

    private void thenVerifyAllACsAreTurnedOn() {
        expectHotelState = "Floor 1\n" +
                "Main corridor 1 Light : OFF  AC : ON \n" +
                "Sub corridor 1 Light : OFF  AC : ON \n" +
                "Sub corridor 2 Light : OFF  AC : ON \n" +
                "Floor 2\n" +
                "Main corridor 1 Light : OFF  AC : ON \n" +
                "Sub corridor 1 Light : OFF  AC : ON \n" +
                "Sub corridor 2 Light : OFF  AC : ON \n";
        assertEquals(expectHotelState, hotel.toString());
    }

    private HotelTest withLightsAndACsInstalled() {
        hotel.install(
                new Hotel.InstallEquipmentInput(Arrays.asList(
                        new Equipment(EquipmentType.LIGHT, 5),
                        new Equipment(EquipmentType.AC, 10)), 1, CorridorType.MAIN, 1));

        hotel.install(
                new Hotel.InstallEquipmentInput(Arrays.asList(
                        new Equipment(EquipmentType.LIGHT, 5),
                        new Equipment(EquipmentType.AC, 10)), 2, CorridorType.MAIN, 1));

        hotel.install(
                new Hotel.InstallEquipmentInput(Arrays.asList(
                        new Equipment(EquipmentType.LIGHT, 5),
                        new Equipment(EquipmentType.AC, 10)), 1, CorridorType.SUB, 1));

        hotel.install(
                new Hotel.InstallEquipmentInput(Arrays.asList(
                        new Equipment(EquipmentType.LIGHT, 5),
                        new Equipment(EquipmentType.AC, 10)), 1, CorridorType.SUB, 2));

        hotel.install(
                new Hotel.InstallEquipmentInput(Arrays.asList(
                        new Equipment(EquipmentType.LIGHT, 5),
                        new Equipment(EquipmentType.AC, 10)), 2, CorridorType.SUB, 1));

        hotel.install(
                new Hotel.InstallEquipmentInput(Arrays.asList(
                        new Equipment(EquipmentType.LIGHT, 5),
                        new Equipment(EquipmentType.AC, 10)), 2, CorridorType.SUB, 2));
        return this;
    }

    private void thenVerifyLightsAreInstalled() {
        assertTrue(hotel.toString().equals("Floor 1\n" +
                "Main corridor 1 Light : OFF \n" +
                "Sub corridor 1\n" +
                "Sub corridor 2\n" +
                "Floor 2\n" +
                "Main corridor 1\n" +
                "Sub corridor 1\n" +
                "Sub corridor 2\n"));
    }

    private void thenVerifyLightsAndACsAreInstalled() {
        assertTrue(hotel.toString().equals("Floor 1\n" +
                "Main corridor 1 Light : OFF  AC : OFF \n" +
                "Sub corridor 1\n" +
                "Sub corridor 2\n" +
                "Floor 2\n" +
                "Main corridor 1\n" +
                "Sub corridor 1\n" +
                "Sub corridor 2\n"));
    }

    private void thenVerifyHotelIsBuildRight() {
        assertNotNull(hotel);
        expectHotelState = "Floor 1\n" +
                "Main corridor 1\n" +
                "Sub corridor 1\n" +
                "Sub corridor 2\n" +
                "Floor 2\n" +
                "Main corridor 1\n" +
                "Sub corridor 1\n" +
                "Sub corridor 2\n";
        assertEquals(expectHotelState, hotel.toString());
    }

    private void whenInstallLightOn(int floor, CorridorType corridorType, int corridorNumber) {
        Equipment lightEquipment = new Equipment(EquipmentType.LIGHT, 5);
        hotel.install(new Hotel.InstallEquipmentInput(Arrays.asList(lightEquipment), floor, corridorType, corridorNumber));
    }

    private void whenInstallLightAndACsOn(int floor, CorridorType corridorType, int corridorNumber) {
        Equipment lightEquipment = new Equipment(EquipmentType.LIGHT, 5);
        Equipment acEquipment = new Equipment(EquipmentType.AC, 10);
        hotel.install(new Hotel.InstallEquipmentInput(Arrays.asList(lightEquipment, acEquipment), floor, corridorType, corridorNumber));
    }

    private void withNoEquipmentsInstalled() {
        assertFalse(hotel.toString().equals("Floor 1\n" +
                "Main corridor 1\n" +
                "Sub corridor 1\n" +
                "Sub corridor 2\n" +
                "Floor 2\n" +
                "Main corridor 1\n" +
                "Sub corridor 1\n" +
                "Sub corridor 2"));
    }

    private HotelTest givenHotel() {
        int numberOfFloors = 2;
        int numberOfMainCorridorsPerFloor = 1;
        int numberOfSubCorridorsPerFloor = 2;

        whenBuildHotelWith(numberOfFloors, numberOfMainCorridorsPerFloor, numberOfSubCorridorsPerFloor);
        return this;
    }

    private void whenBuildHotelWith(int numberOfFloors, int numberOfMainCorridorsPerFloor, int numberOfSubCorridorsPerFloor) {
        hotel = new Hotel.HotelBuilder(numberOfFloors, numberOfMainCorridorsPerFloor, numberOfSubCorridorsPerFloor)
                .build();
    }

    private void thenVerifyHotelIsBuiltWithFloorsAndMainCorridor() {
        assertNotNull(hotel);
        expectHotelState = "Floor 1\n" +
                "Main corridor 1\n" +
                "Floor 2\n" +
                "Main corridor 1\n";
        assertEquals(expectHotelState, hotel.toString());
    }
}