package com.sahajsoft.hotelautomation.automation;

import com.sahajsoft.hotelautomation.equipment.Equipment;
import com.sahajsoft.hotelautomation.equipment.EquipmentType;
import com.sahajsoft.hotelautomation.hotel.CorridorType;
import com.sahajsoft.hotelautomation.hotel.Hotel;
import org.jeasy.rules.api.Rules;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class HotelAutomationTest {

    private HotelAutomation hotelAutomation;
    private Hotel hotel;
    private String hotelCurrentState;
    private String expectedHotelState;
    private Rules rules;

    @Test
    public void shouldGetHotelDefaultState() {
        givenHotelWith(2, 1, 2)
                .andHotelAutomationIsEnabledWithoutAnyRulesConfigured();

        whenRetrieveHotelState();

        thenVerifyHotelDefaultState();
    }

    @Test
    public void shouldTurnLightsOnWhenThereIsMovementInGivenCorridor() {
        givenHotelWith(2, 1, 2)
                .withLightsAndACsInstalled()
                .withActionRuleToTurnOnCorridorLightsDuringNightSlotForMovementRule();

        whenThereIsMovementIn(1, 2, CorridorType.SUB);

        thenVerifyLightIsTurnedOnForMovementOnFloor();

    }

    @Test
    public void shouldTurnOnACsByDefault() {
        givenHotelWith(2, 1, 2)
                .withLightsAndACsInstalled()
                .withDefaultRuleToTurnOnACs();

        whenHotelAutomationTimeSlotIsChanged();
        thenVerifyAllACsAreTurnedOnStartUp();
    }

    @Test
    public void shouldTurnOnMainCorridorLightsDuringNightSlot() {
        givenHotelWith(2, 1, 2)
                .withLightsAndACsInstalled()
                .withActionRuleToTurnOnMainCorridorLightsInNighTimeSlot();

        whenHotelAutomationTimeSlotIsChanged();

        thenVerifyMainCorridorLightsAreTurnedOn();
    }

    @Test
    public void shouldTurnOnCorridorLightsDuringNightSlotWhenThereIsMovementAndTurnOffAfterSometime() {
        givenHotelWith(2, 1, 2)
                .withLightsAndACsInstalled()
                .withActionRuleToTurnOnCorridorLightsDuringNightSlotForMovementRule();

        whenThereIsMovementIn(1, 2, CorridorType.SUB);

        thenVerifyLightIsTurnedOnForMovementOnFloor();
        thenVerifyLightsTurnedOffAfterSometime();
    }

    @Test
    public void shouldTurnOnMainCorridorLightsWhenThereIsMovementAndRemainsOnAfterSubsequentMovement() {
        givenHotelWith(2, 1, 2)
                .withLightsAndACsInstalled()
                .withActionRuleToTurnOnCorridorLightsDuringNightSlotForMovementRule();

        whenThereIsMovementIn(1, 2, CorridorType.SUB);

        thenVerifyLightIsTurnedOnForMovementOnFloor();
        anotherMovementWithGivenTime();
        thenVerifyLightIsTurnedOnForMovementOnFloor();
    }

    @Test
    public void shouldTurnOnCorridorLightsDuringNightSlotWhenThereIsMovementAndTurnOffAfterSometime_AllRules() {
        givenHotelWith(2, 1, 2)
                .withLightsAndACsInstalled()
                .withAllRules();

        whenThereIsMovementIn(1, 2, CorridorType.SUB);

        thenVerifyLightIsTurnedOnForMovementOnFloor_AllRules();
        thenVerifyLightsTurnedOffAfterSometime_AllRules();
    }

    @Test
    public void shouldTurnOnCorridorLightsDuringNightSlotWhenThereIsMovementAndKeptOnForMovement_AllRules() {
        givenHotelWith(2, 1, 2)
                .withLightsAndACsInstalled()
                .withAllRules();

        hotelAutomation = new HotelAutomation(hotel, rules);
        hotelAutomation.run(new HotelAutomation.MovementInput(1, 2, CorridorType.SUB));
        //whenThereIsMovementIn(1, 2, CorridorType.SUB);

        thenVerifyLightIsTurnedOnForMovementOnFloor_AllRules();
        thenVerifyLightsStillOnAfterSometimeDueToAnotherMovement_AllRules();
    }

    @Test
    public void shouldTurnOnCorridorLightsDuringNightSlotWhenThereIsMovementAndTurnOffForMovementOnOtherFloor_AllRules() {
        givenHotelWith(2, 1, 2)
                .withLightsAndACsInstalled()
                .withAllRules();

        hotelAutomation = new HotelAutomation(hotel, rules);
        hotelAutomation.run(new HotelAutomation.MovementInput(2, 2, CorridorType.SUB));

        thenVerifyLightIsTurnedOnForMovementOnDifferentFloors_AllRules();
        thenVerifyLightsStillOnAfterSometimeDueToAnotherMovement_AllRules();
    }

    @Test
    public void shouldEvaluateTotalConsumption(){
        givenHotelWith(2, 1, 2)
                .withLightsAndACsInstalled()
                .withAllRules();

        hotelAutomation = new HotelAutomation(hotel, rules);
        hotelAutomation.run();

        Integer totalConsumption = hotelAutomation.evaluateConsumption();
        assertEquals(new Integer(70), totalConsumption);
    }

    private void anotherMovementWithGivenTime() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        whenThereIsMovementIn(1, 2, CorridorType.SUB);
    }

    private void thenVerifyLightsTurnedOffAfterSometime() {
        try {
            Thread.sleep(7000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
            expectedHotelState = "Floor 1\n" +
                    "Main corridor 1 Light : OFF  AC : OFF \n" +
                    "Sub corridor 1 Light : OFF  AC : OFF \n" +
                    "Sub corridor 2 Light : OFF  AC : OFF \n" +
                    "Floor 2\n" +
                    "Main corridor 1 Light : OFF  AC : OFF \n" +
                    "Sub corridor 1 Light : OFF  AC : OFF \n" +
                    "Sub corridor 2 Light : OFF  AC : OFF \n";
            assertEquals(expectedHotelState, hotelAutomation.retrieveHotelState());
    }

    private void thenVerifyLightsTurnedOffAfterSometime_AllRules() {
        try {
            Thread.sleep(7000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        expectedHotelState = "Floor 1\n" +
                "Main corridor 1 Light : ON  AC : ON \n" +
                "Sub corridor 1 Light : OFF  AC : ON \n" +
                "Sub corridor 2 Light : OFF  AC : ON \n" +
                "Floor 2\n" +
                "Main corridor 1 Light : ON  AC : ON \n" +
                "Sub corridor 1 Light : OFF  AC : ON \n" +
                "Sub corridor 2 Light : OFF  AC : ON \n";
        assertEquals(expectedHotelState, hotelAutomation.retrieveHotelState());
    }

    private void thenVerifyLightsStillOnAfterSometimeDueToAnotherMovement_AllRules() {
        try {
            Thread.sleep(1000);
            hotelAutomation.run(new HotelAutomation.MovementInput(1, 2, CorridorType.SUB));

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        thenVerifyLightIsTurnedOnForMovementOnFloor_AllRules();
    }

    private HotelAutomationTest withActionRuleToTurnOnCorridorLightsDuringNightSlotForMovementRule() {
        rules = new RulesBuilder()
                .turnOnCorridorLightsDuringNightSlotForMovementRule(hotel, HotelAutomation.RuleType.ACTION)
                .build();
        return this;
    }

    private HotelAutomationTest withAllRules() {
        rules = new RulesBuilder()
                .turnOnAllACsRule(hotel, HotelAutomation.RuleType.DEFAULT)
                .turnOnAllMainCorridorLightsDuringNightSlotRule(hotel, HotelAutomation.RuleType.ACTION)
                .turnOnCorridorLightsDuringNightSlotForMovementRule(hotel, HotelAutomation.RuleType.ACTION)
                .build();
        return this;
    }

    private void thenVerifyMainCorridorLightsAreTurnedOn() {
        expectedHotelState = "Floor 1\n" +
                "Main corridor 1 Light : ON  AC : OFF \n" +
                "Sub corridor 1 Light : OFF  AC : OFF \n" +
                "Sub corridor 2 Light : OFF  AC : OFF \n" +
                "Floor 2\n" +
                "Main corridor 1 Light : ON  AC : OFF \n" +
                "Sub corridor 1 Light : OFF  AC : OFF \n" +
                "Sub corridor 2 Light : OFF  AC : OFF \n";
        assertEquals(expectedHotelState, hotelAutomation.retrieveHotelState());
    }

    private void whenHotelAutomationTimeSlotIsChanged() {
        hotelAutomation = new HotelAutomation(hotel, rules);
        hotelAutomation.run();
    }

    private HotelAutomationTest withDefaultRuleToTurnOnACs() {
        rules = new RulesBuilder()
                .turnOnAllACsRule(hotel, HotelAutomation.RuleType.DEFAULT)
                .build();
        return this;
    }

    private HotelAutomationTest withActionRuleToTurnOnMainCorridorLightsInNighTimeSlot() {
        rules = new RulesBuilder()
                .turnOnAllMainCorridorLightsDuringNightSlotRule(hotel, HotelAutomation.RuleType.ACTION)
                .build();
        return this;
    }

    private void thenVerifyAllACsAreTurnedOnStartUp() {
        expectedHotelState = "Floor 1\n" +
                "Main corridor 1 Light : OFF  AC : ON \n" +
                "Sub corridor 1 Light : OFF  AC : ON \n" +
                "Sub corridor 2 Light : OFF  AC : ON \n" +
                "Floor 2\n" +
                "Main corridor 1 Light : OFF  AC : ON \n" +
                "Sub corridor 1 Light : OFF  AC : ON \n" +
                "Sub corridor 2 Light : OFF  AC : ON \n";
        assertEquals(expectedHotelState, hotelAutomation.retrieveHotelState());
    }

    private void whenThereIsMovementIn(int floorNumber, int subCorridorNumber, CorridorType corridorType) {
        hotelAutomation = new HotelAutomation(hotel, rules);
        hotelAutomation.run(new HotelAutomation.MovementInput(floorNumber, subCorridorNumber, corridorType));
    }

    private HotelAutomationTest givenHotelWith(int numberOfFloors, int numberOfMainCorridorsPerFloor, int numberOfSubCorridorsPerFloor) {
        hotel = new Hotel.HotelBuilder(numberOfFloors, numberOfMainCorridorsPerFloor, numberOfSubCorridorsPerFloor)
                .build();
        return this;
    }

    private HotelAutomationTest withLightsAndACsInstalled() {

        hotel.install(
                new Hotel.InstallEquipmentInput(Arrays.asList(
                        new Equipment(EquipmentType.LIGHT, 5),
                        new Equipment(EquipmentType.AC, 10)), 1, CorridorType.MAIN, 1));

        hotel.install(
                new Hotel.InstallEquipmentInput(Arrays.asList(
                        new Equipment(EquipmentType.LIGHT, 5),
                        new Equipment(EquipmentType.AC, 10)), 2, CorridorType.MAIN, 1));

        hotel.install(
                new Hotel.InstallEquipmentInput(Arrays.asList(
                        new Equipment(EquipmentType.LIGHT, 5),
                        new Equipment(EquipmentType.AC, 10)), 1, CorridorType.SUB, 1));

        hotel.install(
                new Hotel.InstallEquipmentInput(Arrays.asList(
                        new Equipment(EquipmentType.LIGHT, 5),
                        new Equipment(EquipmentType.AC, 10)), 1, CorridorType.SUB, 2));

        hotel.install(
                new Hotel.InstallEquipmentInput(Arrays.asList(
                        new Equipment(EquipmentType.LIGHT, 5),
                        new Equipment(EquipmentType.AC, 10)), 2, CorridorType.SUB, 1));

        hotel.install(
                new Hotel.InstallEquipmentInput(Arrays.asList(
                        new Equipment(EquipmentType.LIGHT, 5),
                        new Equipment(EquipmentType.AC, 10)), 2, CorridorType.SUB, 2));
        return this;
    }

    private void thenVerifyHotelDefaultState() {
        expectedHotelState = "Floor 1\n" +
                "Main corridor 1\n" +
                "Sub corridor 1\n" +
                "Sub corridor 2\n" +
                "Floor 2\n" +
                "Main corridor 1\n" +
                "Sub corridor 1\n" +
                "Sub corridor 2\n";
        assertEquals(expectedHotelState, hotelCurrentState);
    }

    private void whenRetrieveHotelState() {
        hotelCurrentState = hotelAutomation.retrieveHotelState();
    }

    private HotelAutomationTest andHotelAutomationIsEnabledWithoutAnyRulesConfigured() {
        hotelAutomation = new HotelAutomation(hotel, new Rules());
        return this;
    }

    private void thenVerifyLightIsTurnedOnForMovementOnFloor() {
        expectedHotelState = "Floor 1\n" +
                "Main corridor 1 Light : OFF  AC : OFF \n" +
                "Sub corridor 1 Light : OFF  AC : OFF \n" +
                "Sub corridor 2 Light : ON  AC : OFF \n" +
                "Floor 2\n" +
                "Main corridor 1 Light : OFF  AC : OFF \n" +
                "Sub corridor 1 Light : OFF  AC : OFF \n" +
                "Sub corridor 2 Light : OFF  AC : OFF \n";
        assertEquals(expectedHotelState, hotelAutomation.retrieveHotelState());
    }

    private void thenVerifyLightIsTurnedOnForMovementOnFloor_AllRules() {
        expectedHotelState = "Floor 1\n" +
                "Main corridor 1 Light : ON  AC : ON \n" +
                "Sub corridor 1 Light : OFF  AC : ON \n" +
                "Sub corridor 2 Light : ON  AC : ON \n" +
                "Floor 2\n" +
                "Main corridor 1 Light : ON  AC : ON \n" +
                "Sub corridor 1 Light : OFF  AC : ON \n" +
                "Sub corridor 2 Light : OFF  AC : ON \n";
        assertEquals(expectedHotelState, hotelAutomation.retrieveHotelState());
    }

    private void thenVerifyLightIsTurnedOnForMovementOnDifferentFloors_AllRules() {
        expectedHotelState = "Floor 1\n" +
                "Main corridor 1 Light : ON  AC : ON \n" +
                "Sub corridor 1 Light : OFF  AC : ON \n" +
                "Sub corridor 2 Light : OFF  AC : ON \n" +
                "Floor 2\n" +
                "Main corridor 1 Light : ON  AC : ON \n" +
                "Sub corridor 1 Light : OFF  AC : ON \n" +
                "Sub corridor 2 Light : ON  AC : ON \n";
        assertEquals(expectedHotelState, hotelAutomation.retrieveHotelState());
    }
}
